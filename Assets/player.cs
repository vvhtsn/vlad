﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {

    private Animator animator;                                                                  // Animator
    private String[,] dirMap;                                                                   // stroes the arrow map
    private int[] currentCell;                                                                  // stores current location of player
    private bool isMoving;                                                                      // boolean for checking if corouting running at the moment
    


    void Start () {
        
        dirMap = null;
        animator = GetComponent<Animator>();
        currentCell = GameObject.Find("Maker").GetComponent<create>().StartNode;
        isMoving = false;

        transform.position = new Vector3(currentCell[0] * 2.5f + 1.25f, currentCell[1] * 2.5f + 1.25f, 0);
    }

    private void Update()
    {

    }

    void FixedUpdate () {
        
        float Horiz = Input.GetAxis("Horizontal");
        float Vert = Input.GetAxis("Vertical");

        if (Horiz > 0.1f)
        {
            MovePlayer(direction.RIGHT);
        }

        if (Horiz < -0.1f)
        {
            MovePlayer(direction.LEFT);
        }

        if (Vert > 0.1f)
        {
            MovePlayer(direction.UP);
        }

        if (Vert < -0.1f)
        {
            MovePlayer(direction.DOWN);
        }

    }

    public enum direction
    {
        UP,
        RIGHT,
        DOWN,
        LEFT,
    }

    public String[,] DirMap
    {
        get
        {
            return dirMap;
        }
        set
        {
            dirMap = value;
        }
    }

    /// <summary>
    /// Function checks map restrictions for movement
    /// and checks for current movement
    /// </summary>
    /// <param name="dir"></param>
    private void MovePlayer(direction dir)
    {
        switch(dir)
        {
            case direction.UP:
                
                if (CanMove('0') && !isMoving)
                {
                    transform.rotation = Quaternion.Euler(-90, 90, -90);
                    StartCoroutine(Movement(new Vector3(currentCell[0], currentCell[1] + 2.5f, 0)));
                }
                break;

            case direction.RIGHT:
                
                if (CanMove('1') && !isMoving)
                {
                    transform.rotation = Quaternion.Euler(0, 90, -90);
                    StartCoroutine(Movement(new Vector3(currentCell[0] + 2.5f, currentCell[1], 0)));
                }
                break;

            case direction.DOWN:
                
                if (CanMove('2') && !isMoving)
                {
                    transform.rotation = Quaternion.Euler(90, 90, -90);
                    StartCoroutine(Movement(new Vector3(currentCell[0], currentCell[1] - 2.5f, 0)));
                }
                break;

            case direction.LEFT:
                
                if (CanMove('3') && !isMoving)
                {
                    transform.rotation = Quaternion.Euler(180, 90, -90);
                    StartCoroutine(Movement(new Vector3(currentCell[0] - 2.5f, currentCell[1], 0)));
                }
                break;

            default:
                break;

        }
    }

    IEnumerator Movement(Vector3 target)
    {
        isMoving = true;


        transform.position = Vector3.MoveTowards(transform.position, target, 1);
        isMoving = false;
        yield return null;
    }

    /// <summary>
    /// Function check allowed directions for player in current cell.
    /// true - movement is allowed,
    /// false - movement is forbidden.
    /// </summary>
    /// <param name="turn"></param>
    /// <returns></returns>
    private bool CanMove(char turn)
    {
        foreach(char arrow in dirMap[ currentCell[0],currentCell[1] ])
        {
            if((turn).Equals(arrow))
            {
                return true;
            }
        }

        return false;
    }
}
